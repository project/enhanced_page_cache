<?php
/**
 * @file
 * Admin Page for the enahcend page cache.
 *
 * @todo BUILD OUR OWN ADMIN INTERFACE
 * - Create Configuration to include / exclude pages from cache.
 * - Create Configuration to configure lifetime by path pattern.
 * - Rules integration to enhance the above mentioned functionality?
 */

/**
 * Settings form.
 */
function enhanced_page_cache_configuration_form($form, &$form_state) {
  $page_cacheability = variable_get('enhanced_page_cache_cacheability', ENHANCED_PAGE_CACHE_CACHEABILITY_NOTLISTED);
  $page_cacheability_pages = variable_get('enhanced_page_cache_cacheability_pages');

  // Per-path cacheability.
  $form['cacheability']['path'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page cacheability'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'cacheability',
    '#weight' => 0,
  );

  $access = user_access('use PHP for settings');
  if ($page_cacheability == ENHANCED_PAGE_CACHE_CACHEABILITY_PHP && !$access) {
    $form['cacheability']['path']['cacheability'] = array(
      '#type' => 'value',
      '#value' => ENHANCED_PAGE_CACHE_CACHEABILITY_PHP,
    );
    $form['cacheability']['path']['pages'] = array(
      '#type' => 'value',
      '#value' => $page_cacheability_pages,
    );
  }
  else {
    $options = array(
      ENHANCED_PAGE_CACHE_CACHEABILITY_NOTLISTED => t('All pages except those listed'),
      ENHANCED_PAGE_CACHE_CACHEABILITY_LISTED => t('Only the listed pages'),
    );
    $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));

    if (module_exists('php') && $access) {
      $options += array(ENHANCED_PAGE_CACHE_CACHEABILITY_PHP => t('Pages on which this PHP code returns <code>TRUE</code> (experts only)'));
      $title = t('Pages or PHP code');
      $description .= ' ' . t('If the PHP option is chosen, enter PHP code between %php. Note that executing incorrect PHP code can break your Drupal site.', array('%php' => '<?php ?>'));
    }
    else {
      $title = t('Pages');
    }
    $form['cacheability']['path']['enhanced_page_cache_cacheability'] = array(
      '#type' => 'radios',
      '#title' => t('Cache specific pages'),
      '#options' => $options,
      '#default_value' => $page_cacheability,
    );
    $form['cacheability']['path']['enhanced_page_cache_cacheability_pages'] = array(
      '#type' => 'textarea',
      '#title' => '<span class="element-invisible">' . $title . '</span>',
      '#default_value' => $page_cacheability_pages,
      '#description' => $description,
    );
  }

  $form['cacheability']['enhanced_page_cache_expiration_mode'] = array(
    '#type' => 'select',
    '#title' => t('Page cache expiration mode'),
    '#default_value' => variable_get('enhanced_page_cache_expiration_mode', CACHE_TEMPORARY),
    '#options' => array(
      CACHE_TEMPORARY => t('Default from standard settings'),
      'lifetime' => t('Lifetime in seconds'),
      'relative' => t('Relative'),
    ),
    '#description' => t('The mode how the lifetime of cached page is set.'),
    '#weight' => 1,
  );
  $enhanced_page_cache_expiration = variable_get('enhanced_page_cache_expiration', CACHE_TEMPORARY);
  $form['cacheability']['enhanced_page_cache_expiration'] = array(
    '#type' => 'textfield',
    '#title' => t('Expiration of cached pages'),
    '#default_value' => $enhanced_page_cache_expiration,
    '#field_suffix' => t(
      '%expire evaluates to %expiration_date',
      array(
        '%expire' => $enhanced_page_cache_expiration,
        '%expiration_date' => date('Y/m/d - H:i:s', enhanced_page_cache_get_expiration_time($enhanced_page_cache_expiration)),
      )
    ),
    '#description' => t('Enter a custom expiration time using integer for seconds or a <a href="http://www.php.net/manual/en/datetime.formats.relative.php">php relative date format</a>.'),
    '#states' => array(
      'invisible' => array(
        ':input[name="enhanced_page_cache_expiration_setting"]' => array('value' => (string) CACHE_TEMPORARY),
      ),
    ),
    '#weight' => 2,
  );

  $form['cacheability']['enhanced_page_cache_join_https'] = array(
    '#type' => 'checkbox',
    '#title' => t('Store http and https pages with the same cache key.'),
    '#default_value' => variable_get('enhanced_page_cache_join_https', TRUE),
    '#description' => t('If enabled the same cache id is used for pages requested by http and https. The module uses <em>hook_file_url_alter()</em> and <em>hook_url_outbound_alter()</em> to ensure protocol relative urls are used. But it is on you to ensure this is globally the case! This functionality just works if you use the default ports 80/443.'),
    '#weight' => 3,
  );

  return system_settings_form($form);
}

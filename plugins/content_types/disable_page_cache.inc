<?php
/**
 * @file
 * Plugin to disable the page cache wherever it is placed.
 */

$plugin = array(
  'title' => t('Disable page cache'),
  'single' => TRUE,
  'icon' => NULL,
  'description' => t('Disables the page cache for this site.'),
  'category' => t('Caching helper'),
  'defaults' => array(),
);

/**
 * Disable the page cache but don't return a thing.
 */
function enhanced_page_cache_disable_page_cache_content_type_render($subtype, $conf, $panel_args) {
  drupal_page_is_cacheable(FALSE);
}

/**
 * Define the settings.
 */
function enhanced_page_cache_disable_page_cache_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  return $form;
}

/**
 * Store the new configuration settings.
 */
function enhanced_page_cache_disable_page_cache_content_type_edit_form_submit($form, &$form_state) {
}

/**
 * Returns the administrative title for a type.
 */
function enhanced_page_cache_disable_page_cache_content_type_admin_title($subtype, $conf, $context) {
  return t('Disable page cache');
}

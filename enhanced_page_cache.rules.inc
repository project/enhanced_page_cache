<?php

/**
 * @file
 * This file provides the rules integration for this module.
 */

/**
 * Implements of hook_rules_action_info().
 */
function enhanced_page_cache_rules_action_info() {
  // Actions that works for everyone.
  $actions = array(
    'enhanced_page_cache_cache_actions_clear_node_page_cache' => array(
      'label' => t('Enhanced page cache: clear page cache of a node'),
      'parameter' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Node'),
          'save' => FALSE,
        ),
      ),
      'group' => t('Cache Actions'),
    ),
    'enhanced_page_cache_cache_actions_clear_page_cache_of_path' => array(
      'label' => t('Enhanced page cache: clear page cache of a system path'),
      'parameter' => array(
        'path' => array(
          'type' => 'text',
          'label' => t('Path to clear.'),
          'description' => t('Use system path or * to flush whole page cache. You can add a path per line.'),
        ),
      ),
      'group' => t('Cache Actions'),
    ),
    'enhanced_page_cache_cache_actions_clear_home_page_cache' => array(
      'label' => t('Enhanced page cache: clear page cache of home'),
      'group' => t('Cache Actions'),
    ),
  );

  if (module_exists('views')) {
    $actions['enhanced_page_cache_actions_clear_views_row_level_cache'] = array(
      'label' => t('Enhanced page cache: clear row level caching of a node'),
      'parameter' => array(
        'view-display' => array(
          'type' => 'list<text>',
          'label' => t('Views'),
          'description' => t('The cache of the node on the views/display will be cleared.'),
          'options list' => '_enhanced_page_cache_rules_get_views_list_uses_panel_fields',
          'save' => FALSE,
          'restriction' => 'input',
        ),
        'row_identifier' => array(
          'type' => 'text',
          'label' => t('The identifier for the row.'),
          'save' => FALSE,
        ),
      ),
      'group' => t('Cache Actions'),
    );
  }

  return $actions;
}


/**
 * Get a a keyed array of views with machine name as key and human readable name
 * as value, that match the condition that they use the enhancedpage_cache panel_fields row plugin.
 *
 * @return array
 *   An array of views names.
 */
function _enhanced_page_cache_rules_get_views_list_uses_panel_fields() {
  $views = views_get_all_views();
  $views_names = array();
  foreach ($views as $view) {
    if ($view->disabled) {
      continue;
    }

    $view->init_display();
    foreach ($view->display as $display_id => $display) {
      $row_plugin = $display->handler->get_option('row_plugin');
      if ($row_plugin == 'cacheable_panels_fields') {
        $views_names[$view->name . ':' . $display->id] = t('@view : @display', array('@view' => $view->name, '@display' => $display->id));
      }
    }
  }
  return $views_names;
}

/**
 * Clear row level caching of a certain view,display,node combination.
 *
 * @todo Clearing this cache does currently ONLY work for the cache actions.
 *
 * Example cid:
 * view-search_shopping-display-context_search_shopping-row-fields-2026
 *
 * @param array $view_displays
 *   Array of View display combinations to flush.
 * @param string $row_identifier
 *   Row identifier - has to be euqal to the content of the field choosen in
 *   the views row plugin setting.
 */
function enhanced_page_cache_actions_clear_views_row_level_cache($view_displays, $row_identifier) {
  foreach ($view_displays as $view_display) {
    list($view_name, $display_id) = explode(':', $view_display);

    $did = 'view-' . $view_name . '-display-' . $display_id;
    $row_cid = $did . '-row-fields-' . $row_identifier;
    cache_clear_all($row_cid, 'cache', TRUE);
  }
}


/**
 * Clear page cache of a node.
 *
 * Clear's the page cache of a node.
 */
function enhanced_page_cache_cache_actions_clear_node_page_cache($node) {
  $cids = array();

  foreach (language_list('language') as $language) {
    $uri = entity_uri('node', $node);
    _enhanced_page_cache_cache_actions_clear_page_cache_create_cids($cids, $uri, $language);
  }
  _enhanced_page_cache_cache_actions_clear_page_cache_cids($cids);
}

/**
 * Clear page cache of home.
 */
function enhanced_page_cache_cache_actions_clear_home_page_cache() {
  enhanced_page_cache_cache_actions_clear_page_cache_of_path('<front>');
}

/**
 * Flushes the page cache of a certain path.
 *
 * @param string $path
 *   Path to flush.
 */
function enhanced_page_cache_cache_actions_clear_page_cache_of_path($paths) {
  $cids = array();

  // Ensure we're dealing with an array.
  if (!is_array($paths)) {
    $paths = explode("\n", $paths);
  }
  // Strip leading / trailing empty chars.
  $paths = array_map('trim', $paths);

  // Now handle all paths.
  foreach ($paths as $path) {
    $path = str_replace('*', '', $path);
    // If path is empty flush whole page cache.
    if (empty($path)) {
      cache_clear_all('*', 'cache_page', TRUE);
      if (module_exists('varnish')) {
        varnish_purge_all_pages();
      }
      return;
    }

    $uri = array(
      'path' => $path,
      'options' => array(
        'https' => FALSE,
        'absolute' => TRUE,
      ),
    );
    foreach (language_list('language') as $language) {
      _enhanced_page_cache_cache_actions_clear_page_cache_create_cids($cids, $uri, $language);
    }
    _enhanced_page_cache_cache_actions_clear_page_cache_cids($cids);
  }
}

/**
 * Flushes a collection of cids.
 *
 * @see _enhanced_page_cache_cache_actions_clear_page_cache_create_cids
 *
 * @param array $cids
 *   Array if cids to flush.
 */
function _enhanced_page_cache_cache_actions_clear_page_cache_cids($cids) {
  if (!module_exists('varnish')) {
    unset($cids['varnish']);
  }
  else {
    $varnish_host = _varnish_get_host();
  }

  $varnish_purge = array();
  foreach ($cids as $type => $cids) {
    foreach ($cids as $cid) {
      if ($type != 'varnish') {
        cache_clear_all($cid, 'cache_page', TRUE);
      }
      else {
        $url = parse_url($cid);
        $host = (isset($url['host'])) ? $url['host'] : $varnish_host;
        $purge = (isset($url['host'])) ? str_replace($url['scheme'] . '://' . $url['host'], '', $cid) : $cid;
        if (substr($purge, 0, 1) != '/') {
          $purge = '/' . $purge;
        }
        // Add proper end-path wildcard: [/]*(\?(.*))*
        // Will find:
        // - /en/node/1
        // - /en/node/1?
        // - /en/node/1?param=1
        // - /en/node/1/
        // - /en/node/1/?
        // - /en/node/1/?param=1
        // But not:
        // - /en/node/1/subpath
        $varnish_purge[$host][] = str_replace('\\', '\\\\', preg_quote($purge)) . '[/]*(\?(.*))*';
      }
    }
  }

  // If there are collected varnish purges build and execute command.
  if (!empty($varnish_purge)) {
    foreach ($varnish_purge as $host => $purge) {
      $purge_pattern = '^(' . implode('|', $purge) . ')$';
      varnish_purge($host, $purge_pattern);
    }
  }
}

/**
 * Returns an array with cids to flush.
 *
 * @param array $cids
 *   The cids array to extend.
 * @param array $uri
 *   The uri array to deal with.
 * @param string $language
 *   The language to use.
 */
function _enhanced_page_cache_cache_actions_clear_page_cache_create_cids(&$cids, $uri, $language = NULL) {
  global $base_insecure_url;
  $cids += array(
    'http' => array(),
    'https' => array(),
    'varnish' => array(),
  );

  if (!is_null($language)) {
    $uri['options']['language'] = $language;
  }
  $uri['options']['absolute'] = TRUE;
  $uri['options']['https'] = FALSE;

  // Fetch all aliases for this path.
  $aliases = $select = db_select('url_alias')
    ->fields('url_alias', array('alias'))
    ->condition('source', $uri['path'])
    ->execute()
    ->fetchCol();

  if (variable_get('enhanced_page_cache_join_https', TRUE)) {
    $cid = url($uri['path'], $uri['options']);
    $vcid = str_replace($base_insecure_url, '', $cid);
    $cids['varnish'][$vcid] = $vcid;
    $cid = preg_replace('/^http(s|):/', '', $cid);
    $cids['http'][$cid] = $cid;

    // Add given path to the alias - just to be sure.
    $aliases[] = $uri['path'];
    foreach ($aliases as $alias) {
      $cid = url($alias, array('alias' => TRUE) + $uri['options']);
      $vcid = str_replace($base_insecure_url, '', $cid);
      $cids['varnish'][$vcid] = $vcid;
      $cid = preg_replace('/^http(s|):/', '', $cid);
      $cids['http'][$cid] = $cid;
    }
  }
  else {
    $cid = url($uri['path'], $uri['options']);
    $vcid = str_replace($base_insecure_url, '', $cid);
    $cids['varnish'][$vcid] = $vcid;
    $cids['http'][$cid] = $cid;
    $cid = url($uri['path'], array('https' => TRUE) + $uri['options']);
    $cids['https'][$cid] = $cid;

    // Add given path to the alias - just to be sure.
    $aliases[] = $uri['path'];
    foreach ($aliases as $alias) {
      $cid = url($alias, array('alias' => TRUE) + $uri['options']);
      $vcid = str_replace($base_insecure_url, '', $cid);
      $cids['varnish'][$vcid] = $vcid;
      $cids['http'][$cid] = $cid;
      $cid = url($alias, array('alias' => TRUE, 'https' => TRUE) + $uri['options']);
      $cids['https'][$cid] = $cid;
    }
  }
}
